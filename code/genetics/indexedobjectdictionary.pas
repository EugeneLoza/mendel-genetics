unit IndexedObjectDictionary;

{$MODE DELPHI}

interface

uses
  Generics.Collections;

type
  TIndexedObjectDictionary<TKey, TValue> = class (TObjectDictionary<TKey, TValue>)
  protected
    function DoRemove(AIndex: SizeInt; ACollectionNotification: TCollectionNotification): TValue; override;
    function DoAdd(constref AKey: TKey; constref AValue: TValue): SizeInt; override;
  public
    KeysList: TList<TKey>;
  public
    constructor Create(AOwnerships: TDictionaryOwnerships); overload;
    constructor Create(AOwnerships: TDictionaryOwnerships; ACapacity: SizeInt); overload;
    destructor Destroy; override;
  end;

implementation
uses
  SysUtils;

function TIndexedObjectDictionary<TKey, TValue>.DoRemove(AIndex: SizeInt; ACollectionNotification: TCollectionNotification): TValue;
begin
  Result := inherited;
  raise Exception.Create('TIndexedObjectDictionary cannot remove items, many thanks to "private" TOpenAddressing.FItems and inconsistent indexing of Generics.Collections. Really, is it all that hard to make important stuff "protected". I''m really tempted to write my own generic dictionary implementation.');
end;

function TIndexedObjectDictionary<TKey, TValue>.DoAdd(constref AKey: TKey; constref AValue: TValue): SizeInt;
begin
  KeysList.Add(AKey);
  Result := inherited;
  //KeyList.Add result <> Result. It's not a bug, it's a "feature", see exception message above.
  Assert(Count = KeysList.Count);
end;

constructor TIndexedObjectDictionary<TKey, TValue>.Create(AOwnerships: TDictionaryOwnerships);
begin
  inherited;
  KeysList := TList<TKey>.Create;
end;
constructor TIndexedObjectDictionary<TKey, TValue>.Create(AOwnerships: TDictionaryOwnerships; ACapacity: SizeInt);
begin
  inherited;
  KeysList := TList<TKey>.Create;
end;

destructor TIndexedObjectDictionary<TKey, TValue>.Destroy;
begin
  KeysList.Free;
  inherited;
end;

end.

