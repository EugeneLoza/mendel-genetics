unit CatCreature;

interface

uses
  GeneticsCore, Gene, GeneSlot, GenePool, GeneFrequency, Genealogy;

type
  TEyeColorGene = class(TGeneSlot)
  public const
    ECG_SILVER = 0;
    ECG_DARK = 1;
    ECG_GRAY = 2;
    ECG_BROWN = 3;
    ECG_GREEN = 4;
    ECG_BLUE = 5;
    ECG_GOLDEN = 6;
    ECG_OCEAN = 7;
    ECG_RED = 8;
  public
    function SayGeneValue(const AGeneValue: TGeneValue): String; override;
    constructor Create; override;
  end;

type
  TCatGenes = class(TGenePool)
  private
    EyeColor: TEyeColorGene;
  public
    RedEyedCat: TGeneFrequency;
    constructor Create; override;
    destructor Destroy; override;
  end;

type
  TCatsGenealogy = class(TGenealogy)
  public
    function CatGenes: TCatGenes;
    function SayCreature(const Id: TCreatureId): String;
    constructor Create; override;
  end;

implementation

function TEyeColorGene.SayGeneValue(const AGeneValue: TGeneValue): String;
begin
  case Round(AGeneValue) of
    ECG_SILVER: Result := 'Silver';
    ECG_DARK: Result := 'Dark';
    ECG_GRAY: Result := 'Gray';
    ECG_BROWN: Result := 'Brown';
    ECG_GREEN: Result := 'Green';
    ECG_BLUE: Result := 'Blue';
    ECG_GOLDEN: Result := 'Golden';
    ECG_OCEAN: Result := 'Ocean';
    ECG_RED: Result := 'Red';
    else
      Result := '?';
  end;
end;
constructor TEyeColorGene.Create;
begin
  inherited;
  GeneSetLength := 2;
  MaxValue := 8;
  MinValue := 0;
  DominanceType := dtDiscrete;
  AddGene(ECG_SILVER, TGene.Create(ECG_SILVER, Random));
  AddGene(ECG_DARK, TGene.Create(ECG_DARK, Random));
  AddGene(ECG_GRAY, TGene.Create(ECG_GRAY, Random));
  AddGene(ECG_BROWN, TGene.Create(ECG_BROWN, Random));
  AddGene(ECG_GREEN, TGene.Create(ECG_GREEN, Random));
  AddGene(ECG_BLUE, TGene.Create(ECG_BLUE, Random));
  AddGene(ECG_GOLDEN, TGene.Create(ECG_GOLDEN, Random));
  AddGene(ECG_OCEAN, TGene.Create(ECG_OCEAN, Random));
  AddGene(ECG_RED, TGene.Create(ECG_RED, Random));
end;

constructor TCatGenes.Create;
begin
  inherited;
  EyeColor := TEyeColorGene.Create;
  GeneSlots.Add(0, EyeColor);
  RedEyedCat := TGeneFrequency.Create;
  RedEyedCat.AddGeneFrequency(0, EyeColor.ECG_RED, 1.0);
end;

destructor TCatGenes.Destroy;
begin
  RedEyedCat.Free;
  inherited;
end;


function TCatsGenealogy.CatGenes: TCatGenes;
begin
  Result := GenePool as TCatGenes;
end;

function TCatsGenealogy.SayCreature(const Id: TCreatureId): String;
begin
  Result := CatGenes.SayGenome(GetCreatureById(Id))
end;

constructor TCatsGenealogy.Create;
begin
  inherited;
  GenePool := TCatGenes.Create;
end;

end.

