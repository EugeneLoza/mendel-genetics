unit GeneFrequency;

interface

uses
  Generics.Collections,
  GeneticsCore;

type
  TGeneFrequencyDictionary = specialize TDictionary<TGeneId, TFloat>;
  TGeneFrequencySlotDictionary = specialize TObjectDictionary<TGeneSlotId, TGeneFrequencyDictionary>;

type
  {}
  { WARNING: Only those GeneSlots that are set are affected!
    If not a single gene has any frequency value for this gene slot,
    then this slot is considered "NIL" and any gene is possible.
    Also if the GeneSlot contains only one possible gene, then
    Frequency of this gene, obviously has no sense, but the algorithm
    performs inefficiently if Frequency < 1.0 }
  TGeneFrequency = class(TObject)
  public
    GeneFrequencies: TGeneFrequencySlotDictionary;
    procedure AddGeneFrequency(const GeneSlot: TGeneSlotId; const Gene: TGeneId; const Frequency: TFloat);
    constructor Create; //virtual//override
    destructor Destroy; override;
  end;

implementation

procedure TGeneFrequency.AddGeneFrequency(const GeneSlot: TGeneSlotId; const Gene: TGeneId; const Frequency: TFloat);
begin
  if not GeneFrequencies.ContainsKey(GeneSlot) then
    GeneFrequencies.Add(GeneSlot, TGeneFrequencyDictionary.Create);
  GeneFrequencies[GeneSlot].Add(Gene, Frequency);
end;

constructor TGeneFrequency.Create;
begin
  inherited;
  GeneFrequencies := TGeneFrequencySlotDictionary.Create([doOwnsValues]);
end;

destructor TGeneFrequency.Destroy;
begin
  GeneFrequencies.Free;
  inherited;
end;

end.

