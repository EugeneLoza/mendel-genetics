unit Genome;

{$mode objfpc}{$H+}

interface

uses
  GeneticsCore, Gene, GeneSlot, GeneFrequency;

type
  { Genetic material of a single creature }
  TGenome = class(TObject) //packed record?
  public
    GeneFrequency: TGeneFrequency; //TGeneFrequencyId?     //Variant (random/born)
    CreatureId: TCreatureId;
    Parents: array of TCreatureId;
    Genes: array of TGeneSet; //packed?
  end;

implementation

end.

