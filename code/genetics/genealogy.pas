unit Genealogy;

{$mode objfpc}{$H+}

interface

uses
  Generics.Collections,
  GeneticsCore, Genome, GenePool, GeneFrequency;

type
  TCreaturesDictionary = specialize TObjectDictionary<TCreatureId, TGenome>;

type
  TGenealogy = class(TObject)
  private
    CreaturesCount: TCreatureId;
    Creatures: TCreaturesDictionary;
    function AddCreature(const ACreature: TGenome): TCreatureId;
  public
    GenePool: TGenePool; //warning: will be freed in Destroy
    function CreateRandomCreature(const AGeneFrequency: TGeneFrequency): TCreatureId;
    function CreateChild(Parents: array of TCreatureId): TCreatureId;
    function GetCreatureById(const Id: TCreatureId): TGenome;
    constructor Create; virtual; //override
    destructor Destroy; override;
  end;

implementation

function TGenealogy.CreateRandomCreature(const AGeneFrequency: TGeneFrequency): TCreatureId;
begin
  Result := AddCreature(GenePool.CreateRandomCreature(AGeneFrequency));
end;

function TGenealogy.CreateChild(Parents: array of TCreatureId): TCreatureId;
var
  ParentGenomes: array of TGenome;
  P: TCreatureId;
begin
  ParentGenomes := nil;
  SetLength(ParentGenomes, Length(Parents));
  for P := 0 to Pred(Length(Parents)) do
    ParentGenomes[P] := Creatures[Parents[P]];
  Result := AddCreature(GenePool.CreateChild(ParentGenomes));
end;

function TGenealogy.GetCreatureById(const Id: TCreatureId): TGenome;
begin
  Result := Creatures[Id];
end;

function TGenealogy.AddCreature(const ACreature: TGenome): TCreatureId;
begin
  Inc(CreaturesCount);
  Creatures.Add(CreaturesCount, ACreature);
  Result := CreaturesCount;
end;

constructor TGenealogy.Create;
begin
  inherited;
  Creatures := TCreaturesDictionary.Create([doOwnsValues]);
  CreaturesCount := 0;
end;

destructor TGenealogy.Destroy;
begin
  Creatures.Free;
  GenePool.Free;
  inherited;
end;

end.

