unit GenePool;

{$mode objfpc}{$H+}

interface

uses
  Generics.Collections,
  GeneSlot, Gene, Genome, GeneticsCore, GeneFrequency;

type
  TGeneSlotsDictionary = specialize TObjectDictionary<TGeneSlotId, TGeneSlot>;

type
  { A species or nation }
  TGenePool = class(TObject)
  public
    GeneSlots: TGeneSlotsDictionary;
    function CreateRandomCreature(const AGeneFrequency: TGeneFrequency): TGenome;
    function CreateChild(Parents: array of TGenome): TGenome;
    function SayGenome(const AGenome: TGenome): String;
    constructor Create; virtual; //override;
    destructor Destroy; override;
  end;

implementation

function TGenePool.CreateRandomCreature(const AGeneFrequency: TGeneFrequency): TGenome;
var
  G: TGeneSlotId;
begin
  Result := TGenome.Create;
  SetLength(Result.Genes, GeneSlots.Count);
  for G in GeneSlots.Keys do
    if (AGeneFrequency <> nil) and (AGeneFrequency.GeneFrequencies.ContainsKey(G)) then
      Result.Genes[G] := GeneSlots[G].RandomGeneSet(AGeneFrequency.GeneFrequencies[G])
    else
      Result.Genes[G] := GeneSlots[G].RandomGeneSet(nil);
  Result.Parents := nil;
  Result.GeneFrequency := AGeneFrequency;
end;

function TGenePool.CreateChild(Parents: array of TGenome): TGenome;
var
  G: TGeneSlotId;
  P: Integer;
  ParentsGeneSets: array of TGeneSet;
begin
  ParentsGeneSets := nil;
  SetLength(ParentsGeneSets, Length(Parents));
  Result := TGenome.Create;
  SetLength(Result.Genes, GeneSlots.Count);
  for G in GeneSlots.Keys do
  begin
    for P := 0 to Pred(Length(Parents)) do
      ParentsGeneSets[P] := Parents[P].Genes[G];
    Result.Genes[G] := GeneSlots[G].BlendGeneSet(ParentsGeneSets);
  end;
  SetLength(Result.Parents, Length(Parents));
  for P := 0 to Pred(Length(Parents)) do
    Result.Parents[P] := Parents[P].CreatureId;
  Result.GeneFrequency := nil;
end;

function TGenePool.SayGenome(const AGenome: TGenome): String;
var
  G: TGeneSlotId;
begin
  Result := '';
  for G in GeneSlots.Keys do
    Result += GeneSlots[G].SayGeneSet(AGenome.Genes[G]);
end;

constructor TGenePool.Create;
begin
  inherited;
  GeneSlots := TGeneSlotsDictionary.Create([doOwnsValues]);
end;

destructor TGenePool.Destroy;
begin
  GeneSlots.Free;
  inherited;
end;

end.

