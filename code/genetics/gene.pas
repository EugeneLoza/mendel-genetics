unit Gene;

interface

uses
  Generics.Collections,
  GeneticsCore;

type
  TDominance = TFloat;
  TGeneValue = TFloat;

type
  TGene = class
  public
    Value: TGeneValue;
    Dominance: TDominance;
    constructor Create(const AValue: TGeneValue; const ADominance: TDominance); //virtual; //override;
  end;

implementation

constructor TGene.Create(const AValue: TGeneValue; const ADominance: TDominance);
begin
  //inherited;
  Value := AValue;
  Dominance := ADominance;
end;

end.

