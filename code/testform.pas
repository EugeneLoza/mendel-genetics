unit TestForm;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation
uses
  GeneticsCore, CatCreature;

{$R *.lfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  Button1Click(nil);
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  Cats: TCatsGenealogy;
  Father, Mother,
    GF_F, GM_F, GF_M, GM_M,
    Child1, Child2, Child3: TCreatureId;
begin
  Randomize;
  Memo1.Clear;

  Cats := TCatsGenealogy.Create;

  GF_F := Cats.CreateRandomCreature(Cats.CatGenes.RedEyedCat);
  GM_F := Cats.CreateRandomCreature(nil);
  GF_M := Cats.CreateRandomCreature(nil);
  GM_M := Cats.CreateRandomCreature(nil);

  Father := Cats.CreateChild([GF_F, GM_F]);
  Mother := Cats.CreateChild([GF_M, GM_M]);
  Child1 := Cats.CreateChild([Father, Mother]);
  Child2 := Cats.CreateChild([Father, Mother]);
  Child3 := Cats.CreateChild([Father, Mother]);

  Memo1.Lines.Add('Child1:' + Cats.SayCreature(Child1));
  Memo1.Lines.Add('Child2:' + Cats.SayCreature(Child2));
  Memo1.Lines.Add('Child3:' + Cats.SayCreature(Child3));
  Memo1.Lines.Add('');
  Memo1.Lines.Add('Father:' + Cats.SayCreature(Father));
  Memo1.Lines.Add('Mother:' + Cats.SayCreature(Mother));
  Memo1.Lines.Add('');
  Memo1.Lines.Add('GF_F:' + Cats.SayCreature(GF_F));
  Memo1.Lines.Add('GM_F:' + Cats.SayCreature(GM_F));
  Memo1.Lines.Add('GF_M:' + Cats.SayCreature(GF_M));
  Memo1.Lines.Add('GM_M:' + Cats.SayCreature(GM_M));

  FreeAndNil(Cats);
end;

end.

