unit GeneSlot;

interface

uses
  Generics.Collections, IndexedObjectDictionary,
  GeneticsCore, Gene, GeneFrequency;

type
  TGenesDictionary = specialize TIndexedObjectDictionary<TGeneId, TGene>;
  TGeneInstances = specialize TObjectDictionary<TGeneInstanceId, TGene>; //Gender cannot be null!

type
  TGeneSet = array of TGeneInstanceId;

type
  TDominanceType = (dtDiscrete, dtAnalog);

type
  TGeneSlot = class(TObject)
  private
    AllGenes: TGenesDictionary;
    InstanceCount: TGeneInstanceId;
    GeneInstances: TGeneInstances;
    GenesCount: TGeneId;
    procedure ValidateGene(const AGene: TGene); virtual;
    procedure ValidateGeneSet(const AGeneSet: TGeneSet); virtual;
  public
    SlotName: String;
    GeneSetLength: Integer;
    DominanceType: TDominanceType;
    MaxValue, MinValue: TGeneValue;
    procedure AddGene(const AGeneId: TGeneId; const AGene: TGene);
    function NewGeneInstance(const AGene: TGene = nil): TGeneInstanceId;
    { Get random gene from all genes in this Slot
      Passing nil will make all genes have equal chances }
    function RandomGene(const GeneFrequency: TGeneFrequencyDictionary): TGene;
    function RandomGeneSet(const GeneFrequency: TGeneFrequencyDictionary): TGeneSet;
    function BlendGeneSet(GeneSets: array of TGeneSet): TGeneSet;
    function GeneSetValue(const AGeneSet: TGeneSet): TGeneValue;
    function SayGeneValue(const AGeneValue: TGeneValue): String; virtual;
    function SayGene(const AGene: TGene): String;
    function SayGeneSet(const AGeneSet: TGeneSet): String;
    constructor Create; virtual; //override
    destructor Destroy; override;
  end;

implementation

procedure TGeneSlot.ValidateGene(const AGene: TGene);
begin
  GeneticsAssert(AGene.Value <= MaxValue, 'TGeneSlot.ValidateGene');
  GeneticsAssert(AGene.Value >= MinValue, 'TGeneSlot.ValidateGene');
end;

procedure TGeneSlot.ValidateGeneSet(const AGeneSet: TGeneSet);
var
  I: Integer;
begin
  GeneticsAssert(Length(AGeneSet) = GeneSetLength, 'TGeneSlot.ValidateGeneSet:length');
  for I := 0 to Pred(GeneSetLength) do
    GeneticsAssert(AGeneSet[I] > 0, 'TGeneSlot.ValidateGeneSet:GeneId');
end;

procedure TGeneSlot.AddGene(const AGeneId: TGeneId; const AGene: TGene);
begin
  ValidateGene(AGene);
  GeneticsAssert(not AllGenes.ContainsKey(AGeneId));
  Inc(GenesCount);
  AllGenes.Add(AGeneId, AGene);
end;

function TGeneSlot.NewGeneInstance(const AGene: TGene = nil): TGeneInstanceId;
begin
  Inc(InstanceCount);
  GeneInstances.Add(InstanceCount, AGene);
  Result := InstanceCount;
  GeneticsAssert(InstanceCount = GeneInstances.Count, 'TGeneSlot.NewGeneInstance');
end;

function TGeneSlot.RandomGene(const GeneFrequency: TGeneFrequencyDictionary): TGene;
var
  GId: TGeneId;
begin
  GeneticsAssert(AllGenes.Count > 0, 'TGeneSlot.RandomGene');
  repeat
    GId := Rnd(AllGenes.KeysList.Count);
  until (GeneFrequency = nil) or ((GeneFrequency.ContainsKey(GId)) and (GeneFrequency[GId] > Random));
  Result := AllGenes[AllGenes.KeysList[GId]];
end;

function TGeneSlot.RandomGeneSet(const GeneFrequency: TGeneFrequencyDictionary): TGeneSet;
var
  I: Integer;
begin
  Result := Default(TGeneSet);
  SetLength(Result, GeneSetLength);
  for I := 0 to Pred(GeneSetLength) do
    Result[I] := NewGeneInstance(RandomGene(GeneFrequency));
end;

function TGeneSlot.BlendGeneSet(GeneSets: array of TGeneSet): TGeneSet;
var
  SourceGeneSet: TGeneSet;
  I, J, K: Integer;
begin
  for J := 0 to Pred(Length(GeneSets)) do
    ValidateGeneSet(GeneSets[J]);
  SourceGeneSet := Default(TGeneSet);
  SetLength(SourceGeneSet, GeneSetLength * Length(GeneSets));
  for J := 0 to Pred(Length(GeneSets)) do
    for I := 0 to Pred(GeneSetLength) do
      SourceGeneSet[I + GeneSetLength * J] := GeneSets[J][I];
  I := 0;
  Result := Default(TGeneSet);
  SetLength(Result, GeneSetLength);
  repeat
    K := Rnd(Length(SourceGeneSet));
    if SourceGeneSet[K] > 0 then
    begin
      Result[I] := SourceGeneSet[K];
      SourceGeneSet[K] := 0; // a dirty and probably more efficient way to avoid using duplicate genes - maybe removing a record from SourceGeneSet would be more efficient, needs profiling
      Inc(I);
    end;
  until I = GeneSetLength;
  ValidateGeneSet(Result);
end;

function TGeneSlot.GeneSetValue(const AGeneSet: TGeneSet): TGeneValue;
var
  I: Integer;
  D: TDominance;
begin
  case DominanceType of
    dtDiscrete:
      begin
        D := -1;
        for I := 0 to Pred(GeneSetLength) do
          if GeneInstances[AGeneSet[I]].Dominance > D then
          begin
            D := GeneInstances[AGeneSet[I]].Dominance;
            Result := GeneInstances[AGeneSet[I]].Value;
          end;
      end;
    dtAnalog:
      begin
        D := 0;
        Result := 0;
        for I := 0 to Pred(GeneSetLength) do
          begin
            D += GeneInstances[AGeneSet[I]].Dominance;
            Result += GeneInstances[AGeneSet[I]].Value * GeneInstances[AGeneSet[I]].Dominance;
          end;
        Result := Result / D;
      end;
  end;
end;

function TGeneSlot.SayGeneValue(const AGeneValue: TGeneValue): String;
begin
  Result := ValueToStr(AGeneValue, 10);
end;

function TGeneSlot.SayGene(const AGene: TGene): String;
begin
  Result := SayGeneValue(AGene.Value) + ':' + ValueToStr(AGene.Dominance, 10);
end;

function TGeneSlot.SayGeneSet(const AGeneSet: TGeneSet): String;
var
  I: Integer;
begin
  Result := SayGeneValue(GeneSetValue(AGeneSet)) + '(';
  for I := 0 to Pred(GeneSetLength) do
  begin
    Result += SayGene(GeneInstances[AGeneSet[I]]);
    if I <> Pred(GeneSetLength) then
      Result += ',';
  end;
  Result += ')';
end;

constructor TGeneSlot.Create;
begin
  inherited;
  AllGenes := TGenesDictionary.Create([doOwnsValues]);
  GeneInstances := TGeneInstances.Create([]);
  InstanceCount := 0;
  GenesCount := 0;
end;

destructor TGeneSlot.Destroy;
begin
  GeneInstances.Free;
  AllGenes.Free;
  inherited;
end;

end.

