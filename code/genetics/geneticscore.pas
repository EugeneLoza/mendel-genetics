unit GeneticsCore;

interface

type
  TFloat = Single;

type
  TGeneSlotId = byte; //max 255 genes per creature
  TGeneId = Word; //max 65535 different possible genes per slot
  TGeneInstanceId = LongWord; //max 4294967295/2 random creatures supported (2 bln.)
  TCreatureId = LongWord; //max 4294967295 total creatures supported (4 bln.)

procedure GeneticsAssert(const AValue: Boolean; const Message: String = '');
function ValueToStr(const AValue: TFloat; const Digits: Integer = 1): String;
function Rnd(const N: Integer): Integer;
function Rnd: TFloat;

implementation
uses
  SysUtils;

type
  EGeneticsException = Class(Exception);

procedure GeneticsAssert(const AValue: Boolean; const Message: String = '');
begin
  if not AValue then
    raise EGeneticsException.Create('Genetics assertion failed! ' + Message);
end;

function ValueToStr(const AValue: TFloat; const Digits: Integer = 1): String;
begin
  Result := FloatToStr(Round(AValue * Digits) / Digits);
end;

function Rnd(const N: Integer): Integer;
begin
  Result := Random(N);
end;

function Rnd: TFloat;
begin
  Result := Random;
end;

end.

